# Exercice 5
# Example de phrase: what time do i have to be there ? what is your address ?
# Example de résultat

phrase_end_characters = "?.!"


def capitalize_i(text):
    return text.replace(" i ", " I ")


def capitalize_end_phrase(text):
    result = ""
    new_phrase = False
    for word in text.split(" "):
        if new_phrase:
            result += word.capitalize() + " "
            new_phrase = False
        else:
            result += word + " "
        if word in phrase_end_characters:
            new_phrase = True
    return result[0].upper() + result[1:]


def capitalize(text):
    return capitalize_end_phrase(capitalize_i(text))


print(str(capitalize(input("Entrez votre phrase à formater: "))))
