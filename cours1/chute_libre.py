# Exercice 2

from math import sqrt

GRAVITY_ACCELERATION = 9.8


def getFinalVelocity(d):
    return sqrt(2 * GRAVITY_ACCELERATION * d)


distance = float(input("Entrez la distance entre l'objet et le sol: "));

print("La vitesse final lors de l'impact entre l'objet et le sol est de %.2f" % (
    getFinalVelocity(distance)))
