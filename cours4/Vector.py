class Vector:
    def __init__(self, x, y):
        self.__x = x
        self.__y = y

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    @x.setter
    def x(self, x):
        if x > 0:
            self.__x = x

    @y.setter
    def y(self, y):
        if y > 0:
            self.__y = y

    def __add__(self, other):
        x = self.__x + other.x
        y = self.__y + other.y
        return Vector(x, y)

    def __str__(self):
        return f"[{self.__x}, {self.__y}]"


v1 = Vector(2, 2)
v1.x = 8
v1.y = -4
print(v1)

v2 = Vector(3, 3)
print(v2)

v3 = v1 + v2
print(v3)
