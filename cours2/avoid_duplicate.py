# Exercise 7

# Take input words from user and insert them into a list while avoiding duplicates

print("Enter words")
word = input()
uniqueWords = []
while word != "":
    if word not in uniqueWords:
        uniqueWords.append(word)
    word = input()
print(uniqueWords)
