path = ""
myInput = input("Enter a number: \n")

try:
    myFile = open(path, 'w')
    myFile.write("hello world!")
    value = int(myInput) / 0
    myFile.write(str(value))

    if type(myInput) is str:
        raise TypeError

except FileNotFoundError as fnfe:
    print("A FileNotFound error was raised")
    print(fnfe)
except TypeError as te:
    print("A TypeError error was raised")
    print(te)
except ZeroDivisionError as zde:
    print("A ZeroDivisionError error was raised")
    print(zde)
except Exception as e:
    print("Unknown error")
    print(e)
finally:
    print("")
