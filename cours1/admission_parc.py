# Exercice 4

BABY_LIMIT = 2.00
BABY_PRICE = 0.00

CHILD_LIMIT = 12
CHILD_PRICE = 23.00

ADULT_LIMIT = 64
ADULT_PRICE = 23.00

SENIOR_PRICE = 18.00

price = 0
current_age = input("Entrez l'age de la personne: ")

while current_age != "":
    current_age = int(current_age)
    if current_age <= BABY_LIMIT:
        price += BABY_PRICE
    elif current_age <= CHILD_PRICE:
        price += CHILD_PRICE
    elif current_age <= ADULT_LIMIT:
        price += ADULT_LIMIT
    else:
        price += SENIOR_PRICE
    current_age = input("Entrez l'age de la personne: ")

print("Prix total: %.2f" % price)
