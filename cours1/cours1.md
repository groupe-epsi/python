# Cours 1

## Bases du langage

### Déclarer une fonction: def

	def myfunc(monparam):
		return len(monparam)

### Pour précompiler:

	import dis

	dis.dis(myfunc);

### Types:

int, bool, float, complexe
Itérable: str, list, tuple, set, dict (object)
None

### Opérateurs:

\**, pow, sqrt, abs
is, is not pour la comparaison de types
or and not


### Déclaration des variables:

	age = 46
	age2 = age

	id(age) # 2910253770376
	id(age2) # 2910253770376
	type(age) # int

### Built-int functions:

	pi = 3.14

	"qsdsq".capitalize(); # Qsdsq
	len("qsdqs") # 5
	int(pi) # 3

### Chaine de caractères:

	version = 1.0.0
	Interpolation: f"Hello version {version}" # Hello version 1.0.0
	Concatenation "Hello version" + str(version)

### Slicing:

[debut,taille,pas]
	resultat = "intervention"[0:4] # inte
	resultat = "intervention"[0:6:2] # itr
	resultat = "intervention"[-3:] # ion
	resultat = "intervention"[::-1] # noitnevretni

### Structure conditionelle:

	if condition: 
		print("Condition est vrai")
	print("Fin")

	if condition:
		print("Condition est vrai")
	else:
		print("Condition est fausse")

**Pas de switch:** 

	if choix == 1:
		# bloc 1
	elif choix == 2:
		# bloc 2
	elif choix == 3:
		# bloc 3
	else:
		# bloc 4

**Version compact:**

	y = x\**2 if x > 0 else y = x/4

### Structure de répétition: 
	
** while: **

	i = 0
	while i < 4:
		i++;
		if i==2:
			break;

** for: **

	lst = [1, 2, 3, 4]
	for element in list:
		print(element)