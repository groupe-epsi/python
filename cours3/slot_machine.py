import random


# Create an array containing lines of symbols,
# the width and height depends of the number of symbols
# For example, this methods will return a 3x3 array for symbols 🎁⚽❤
def create_slot_machine(symbols):
    lines = []
    for row in range(len(symbols)):
        lines.append(symbols.copy())
    return lines


# Shuffle the symbols
def shuffle(slot_machine):
    for line in slot_machine:
        random.shuffle(line)


# Check if player has won
# A player wins when the slot machine contains a line of the specified symbol
def check_for_win(slot_machine, symbol):
    for column in range(len(slot_machine)):
        symbol_count = 0
        for row in range(len(slot_machine)):
            if slot_machine[row][column] == symbol:
                symbol_count += 1
        if symbol_count == len(slot_machine):
            return True
    return False


# Display a slot machine to console
# Symbols in the slot machine are arranged in lines
# eg:   ['❤','⚽','🎁']
#       ['🎁','⚽','❤']
#       ['❤','🎁','⚽']
# So we need to display the slot machine in columns
def display_slot_machine(slot_machine):
    for column in range(len(slot_machine)):
        for row in range(len(slot_machine)):
            print(slot_machine[row][column], end="")
        print()


# Display a message and wait for an integer input
# The input should be higher than minimum and less than maximum
# The input will be asked again until it is valid
def get_player_input(prompt, minimum=0, maximum=20):
    player_input = ""
    while not type(player_input) == int or player_input < minimum or player_input > maximum:
        try:
            player_input = int(input(prompt))
        except ValueError:
            print(f"Please enter a value between {minimum} and {maximum}")
    return player_input


SYMBOLS = ['😁', '🍉', '⚽', '🎁']

playerCount = get_player_input("Enter the number of players: ", 1, 8)

slotMachines = []
for playerIndex in range(playerCount):
    slotMachines.append(create_slot_machine(SYMBOLS))

playerWon = False
winnerIndex = 0
currentRound = 0
while not playerWon:
    for playerIndex in range(playerCount):
        for char in range(len(SYMBOLS)):
            print(f'{char + 1} {SYMBOLS[char]}')

        # Get a number between 0 and the max number of symbols
        characterIndex = get_player_input(f"Player {playerIndex + 1}, please pick a symbol: ", 0, len(SYMBOLS)) - 1

        shuffle(slotMachines[playerIndex])
        display_slot_machine(slotMachines[playerIndex])
        if check_for_win(slotMachines[playerIndex], SYMBOLS[characterIndex]):
            playerWon = True
            break
    currentRound += 1
print(f"Congratulations, Player {winnerIndex + 1} won in {currentRound} rounds")