# Dictionary creation

emptyDictionary = {}
objectDictionary = dict()
dictionaryWithInitialValues = {
    "name": "Laurent",
    "age": 22,
    "id": "S656255"
}

emptyDictionary["name"] = "Florent"
print(emptyDictionary)  # {'name': 'Florent'}
print(dictionaryWithInitialValues)  # {'name': 'Laurent', 'age': 22, 'id': 'S656255'}

# Dictionary attributes

print(dictionaryWithInitialValues.keys())  # dict_keys(['name', 'age', 'id'])
print(dictionaryWithInitialValues.values())  # dict_values(['Laurent', 22, 'S656255'])

if "id" in dictionaryWithInitialValues:
    print(dictionaryWithInitialValues["id"])  # S656255

for key, value in dictionaryWithInitialValues.items():
    print(key, ":", value)