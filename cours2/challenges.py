# Challenge 1
# Display pairs in a list

rangeList = range(30)
print([x for x in rangeList if x % 2 == 0])

# Challenge 2
# Display every words in uppercase along with its length

sentence = "Python is way too powerful"
print([(word.upper(), len(word)) for word in sentence.split(" ")])
