# Exercice 3

cotes = [
    float(input("Longeur côté 1: ")),
    float(input("Longeur côté 2: ")),
    float(input("Longeur côté 3: "))
]


def is_equilateral():
    return cotes[0] == cotes[1] == cotes[2]


def is_isosceles():
    return cotes[0] == cotes[1] or cotes[0] == cotes[2] or cotes[1] == cotes[2]


if is_equilateral:
    print("Le triangle est équilatéral")
elif is_isosceles:
    print("Le triangle est isocèle")
else:
    print("Le triangle est scalène")
