import os
from datetime import datetime

TXT = "epsi_error.txt"
PATH = "c:\\temp"


class EpsiException(Exception):
    def __init__(self, exception, error_code, error_message):
        super().__init__(exception)
        self.__inner_exception = exception
        self.__error_code = error_code
        self.__error_message = error_message
        os.chdir(PATH)
        self.__log_in_file()

    def __log_in_file(self):
        error_file = open(TXT, "a")
        error_file.write(self.__format_error())
        error_file.close()

    def __format_error(self):
        return f"{datetime.datetime.now()}; {self.__inner_exception}; {self.__error_code}; {self.__error_message}"


try:
    print("hello")
    i = 5
    k = i + 'kkk'
    a = 5 / 0
    raise EpsiException
except Exception:
    print("Exception occured")
