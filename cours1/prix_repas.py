# Exercice 1

TAX_RATE = .1
TIP_RATE = .18

prix = float(input("Veuillez entrer le prix d'un repas: "))

tax = prix * TAX_RATE
tip = prix * TIP_RATE
total = prix + tax + tip

print(" La TVA est de %.2f, le pourboire de %.2f pour un total de %.2f" % (tax, tip, total))
