# List creation
print("List creation \n")

bracketList = []
objectList = list()

listWithInitialValues = [10, 20, 30, 40]
genericList = [34, 'hello', (4, 8), [1, 2, 3]]
rangeList = range(30)
repeatList = [0] * 5

print(rangeList)  # 0 to 30
print(repeatList)  # 00000

print()
# List modification
print("List modification \n")

sampleList = [3, 6, 8, 42, 12, 96]
firstValue = sampleList[0]

concatList = listWithInitialValues + sampleList
print(concatList)  # 10, 20, 30, 40, 3, 6, 8, 42, 12, 96

copyList = concatList  # Copy by reference !!
copyList[0] = 0
print(copyList)  # 0, 20, 30, 40, 3, 6, 8, 42, 12, 96
print(concatList)  # 0, 20, 30, 40, 3, 6, 8, 42, 12, 96

sampleList.append(7)
print(sampleList)  # 3, 6, 8, 42, 12, 96, 7

sampleList.sort()
print(sampleList)  # 3, 6, 7, 8, 12, 42, 96

sampleList.reverse()
print(sampleList)  # 96, 42, 12, 8, 7, 6, 3

indexOfValue = sampleList.index(7)
print(indexOfValue)  # 4

sampleList.remove(8)
print(sampleList)  # 96, 42, 12, 7, 6, 3

lastValue = sampleList.pop()
print(lastValue)  # 3
print(sampleList)  # 96, 42, 12, 7, 6

print()
# List looping
print("List looping \n")

charList = ['u', 's', 'e', 'r', 1, 5, 6]
result = ""

for c in charList:
    if type(c) == str:
        result += c

print(result)  # user

print()
# List comprehension
print("List comprehension \n")

sampleList = [56, 2, 64, 9, 23, 985, 12]
squareList = [x ** 2 for x in sampleList if x > 15]
print(squareList)  # 3136, 4096, 529, 970225

print()
# Mapping
print("Mapping")

sampleList = [56, 58, 12, 69, 4]


def cube(x):
    return x ** 3


cubeList = list(map(cube, sampleList))
print(cubeList)  # 175616, 195112, 1728, 328509, 64
print([cube(x) for x in sampleList])  # same result
