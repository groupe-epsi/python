class User:

    def __init__(self, identifier, firstname, password):
        self.identifier = identifier
        self.firstname = firstname
        self.password = password

    def validate_password(self, password):
        return self.password == password

    def __str__(self):
        return f"{self.firstname} - {self.identifier}"


user = User("LNF", "Biden", "1234")
user.nom = "Bob"
user.id = "S125"

print(user.validate_password("1234"))
print(user)
