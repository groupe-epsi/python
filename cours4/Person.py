class Person:
    def __init__(self, firstname, lastname, age):
        self.firstname = firstname
        self.lastname = lastname
        self.age = age

    def is_adult(self):
        return self.age >= 18


class Employee(Person):
    def __init__(self, firstname, lastname, age, company):
        super().__init__(firstname, lastname, age)
        self.company = company
        self.salary = 1500

    def raise_salary(self, amount):
        self.salary += amount


firstPerson = Person("Eprinchard", "Jean", 48)
print(firstPerson.is_adult())

firstEmployee = Employee("Petit", "Remi", 19, "Microsoft")
print(firstEmployee.is_adult())

print(firstEmployee.salary)
firstEmployee.raise_salary(1000)
print(firstEmployee.salary)
